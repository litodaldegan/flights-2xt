import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    # ...
    SQLALCHEMY_DATABASE_URI = "postgresql://{0}:{1}@{2}/{3}".format(
        'admin',
        '123456',
        'localhost',
        'flights',
    )	
    SQLALCHEMY_TRACK_MODIFICATIONS = False