from app import db

class Airport(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    iata = db.Column(db.String(3), unique=True)
    city = db.Column(db.String(50))
    lat = db.Column(db.Float)
    lon = db.Column(db.Float)
    state = db.Column(db.String(2))

    def __repr__(self):
    	return '<Airport: %r>' % self.iata

    def __init__(self, *initial_data, **kwargs):
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])

class Flight(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    departure_time = db.Column(db.DateTime)
    arrival_time = db.Column(db.DateTime)
    fare_price = db.Column(db.Float)
    currency = db.Column(db.String(3))
    requested_url = db.Column(db.String(128))
    aircraft_id = db.Column(db.Integer, db.ForeignKey('aircraft.id'))
    origin_airport_id = db.Column(db.Integer, db.ForeignKey("airport.id"))
    destiny_airport_id = db.Column(db.Integer, db.ForeignKey("airport.id"))

    origin_airport = db.relationship("Airport", foreign_keys=origin_airport_id)
    destiny_airport = db.relationship("Airport", foreign_keys=destiny_airport_id)
    aircraft = db.relationship("Aircraft", foreign_keys=aircraft_id)

    def __repr__(self):
    	return '<Departure: %r - Arrival: %r - Currency: %r - Aircraft: %r>' % (self.departure_time, 
    		self.arrival_time, self.currency, self.aircraft)

    def __init__(self, *initial_data, **kwargs):
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])

class Aircraft(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    model = db.Column(db.String(64))
    manufacturer = db.Column(db.String(64))
    esteemed_speed = db.Column(db.Float, default=0)
    esteemed_fare_price = db.Column(db.Float, default=0)

    __table_args__ = (db.UniqueConstraint('model', 'manufacturer'),)

    def __repr__(self):
    	return '<Model: %r - Manufacturer: %r>' % (self.model, self.manufacturer)

    def __init__(self, *initial_data, **kwargs):
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])

class Airports_Distance(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    origin_airport_id = db.Column(db.Integer, db.ForeignKey("airport.id"))
    destiny_airport_id = db.Column(db.Integer, db.ForeignKey("airport.id"))
    distance = db.Column(db.Float, default=0)

    origin_airport = db.relationship("Airport", foreign_keys=origin_airport_id)
    destiny_airport = db.relationship("Airport", foreign_keys=destiny_airport_id)

    __table_args__ = (db.UniqueConstraint('origin_airport_id', 'destiny_airport_id'),)
    
    def __init__(self, *initial_data, **kwargs):
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])

class Stretch_Cheapest_Option(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    origin_airport_id = db.Column(db.Integer, db.ForeignKey("airport.id"))
    destiny_airport_id = db.Column(db.Integer, db.ForeignKey("airport.id"))
    flight_id = db.Column(db.Integer, db.ForeignKey("flight.id"))

    origin_airport = db.relationship("Airport", foreign_keys=origin_airport_id)
    destiny_airport = db.relationship("Airport", foreign_keys=destiny_airport_id)
    flight = db.relationship("Flight", foreign_keys=flight_id)

    def __init__(self, *initial_data, **kwargs):
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])