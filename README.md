# flights-2xt
Ítalo Daldegan de Oliveira

## Introdução
Esta é uma pequena aplicação que consume dados de voo da API de MOCK da [2XT](https://www.2xt.com.br/).
Principal objetivo desta aplicação é atestar conhecimento em [python3](https://www.python.org/) e [flask](http://flask.pocoo.org/).

## Ambiente

A aplicação toda foi densenvolvida fazendo uso do sistema operacional linux e a linguagem de programação python.
Para oferecer as funções básica de rota e redirecionamentos foi usado o framework [flask](http://flask.pocoo.org/). A escolha deste foi dado pela experiência prévia e por sua simplicidade.
Com o objetivo de facilitar a gestão das dependências do projeto foi usado o gerênciador de pacotes [PIP](https://pip.pypa.io/en/stable/).
Para isolar o sistema da instação das biblioteca foi utilizado o [virtual](https://virtualenv.pypa.io/en/stable/). Para facilitar o uso de ambiente virtuais foi usado o [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/). 

### Banco de dados

Para que a aplicação possa armazenar os dados coletados/calculados e funcione corretamente, é necessário que seja criado uma _database_ no banco local chamada _flights_. É necesário também a criação de um usuário chamado "admin" com a senha "123456", tendo este direito de escrita e leitura à _database_. Estas configurações, de acesso ao banco, estão presentes no arquivo config.py e não foram colocadas como variáveis de ambiente somente por motivo de simplicidade da aplicação. 

### Variáveis de ambiente

Para o correto funcionamento da aplicação, devem ser configurada as seguinte variáveis de ambiente:

```
export FLASK_APP=flights.py
export AIRPORTS_DATA_URL=http://stub.2xt.com.br/air/airports/prLxyWvYOvwmvlDrJMENtmLgmvXmOzt4
export USER=
export PASSWORD=
export APIKEY=
```

### Executando a aplicação

Para executar a aplicação, tendo as variáveis de ambiente configuradas corretamente, o primeiro passo consiste na instalação das bibliotecas requeridas pela aplicação, isto pode ser feito utilizado (no caso do PIP) com o seguinte comando:

```
pip install -r requirementes.txt
```

Tendo sucesso na instalação das bibliotecas o próximo passo consiste em criar as tabelas na _database_. Isto pode ser feito rodando o seguinte comando:

```
flask db upgrade
```

Após a criação das tabela basta executar o flask:

```
flask run
```

Com isto, basta acessar o seguinte endereço, por meio de um navegador, para cair na página inicial da aplicação:

```
http://localhost:5000/
```

## Planejamento
Esta seção tem por objetivo descrever as principais atividades a serem realizadas com o objetivo de criar uma pequena aplicação que deve fornecer algumas informações de voos usando como base a API de testes da 2XT. Também é objetivo estabelecer um planejamento de tempo necessário para implementação de cada etapa do processo.

### Tarefas
Na tabela abaixo estão listadas as principais atividades, a nível macro, identificadas para a criação do projeto. Para cada tarefa foi previsto um tempo médio de execução e uma data para a realização.

Tarefa | Tempo estimado | Data para realização
------|---------------|--------------------
Estudar o problema e propor uma solução | 1 hora | 27/06/2018
Criar o ambiente virtual de desenvolvimento | 1/2 hora | 27/06/2018
Estudar opções de biblioteca para comunicação com PostgreSQL e fazer as configurações inicias | 1 hora | 30/06/2018
Criar a _database_ para o projeto | 1 hora | 30/06/2018
Declarar o _models_ para os dados | 1/2 hora | 30/06/2018
Função para carregar os dados de aeroportos no banco | 1 hora | 30/06/2018
Procedimento que busca e popula o banco com os dados de voo dos último 40 dias | 2 horas | 30/08/2018
Método que dado duas latitudes retorna a distância | 1 hora | 02/07/2018
Função que calcula a velocidade média e tarifa média para cada modelo de aeronave | 4 horas | 02/07/2018 
Procedimento para calcular dados básico de aeroportos (distância, preços e aeronave da passagem de menor valor) | 4 horas | 04/07/2018
Criar _controler_ e _view_ para exibir a análise dos dados como maior distância entre aeroportos em KM, viagem com maior duração etc | 2 horas | 06/07/2018
Criar _controler_ e _view_ para retornar as informações de voo para uma origem, destino e data | 3 horas | 07/07/2018
Descrevendo brevemente a solução adotada, procedimento para executar a aplicação e decisões de implementação | 2 horas | 07/07/2018
Tempo total estimado | 23 horas | 

Dado o tempo total estimado e pensando em uma margem de erro e imprevistos que podem atrasar a execução das tarefas, é proposto a entrega do projeto no dia 08/07/2018.

## Referências

Como criar um objeto a partir de um dicionário:
https://stackoverflow.com/questions/2466191/set-attributes-from-dictionary-in-python

Tutorial de como criar uma aplicação em flask:
https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-iv-database

Fazer requisição HTTP:
http://docs.python-requests.org/en/master/user/quickstart/

Documentação do Fask:
http://flask.pocoo.org/docs/1.0/quickstart/

Documentação do Jinja2:
http://jinja.pocoo.org/docs/2.10/templates/