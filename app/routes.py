import json, requests, os, geopy.distance
from app import app, db
from datetime import datetime
from app.models import Airport, Flight, Aircraft, Airports_Distance, Stretch_Cheapest_Option
from sqlalchemy import and_, or_, func
from datetime import timedelta
from flask import render_template, redirect, url_for

@app.route('/')
@app.route('/index')
def index():
    airports = Airport.query.all()
    flights = Flight.query.all()
    aircrafts = Aircraft.query.all()
    distances = Airports_Distance.query.filter().order_by(Airports_Distance.distance.desc())
    nAirports = len(airports)
    nFlights = len(flights)
    nAircrafts = len(aircrafts)
    nDistances = distances.count()
    expensive_flight = None
    cheapest_flight = None
    longest_stretch = None
    longest_stretch_apo = None
    longest_stretch_apd = None
    longest_stretch_flight = None 
    longest_stretch_aircraft = None 

    if nAirports == 0:
        nAirports = load_airports_data()

    if nFlights != 0:
        expensive_flight = db.session.query(func.max(Flight.fare_price)).scalar()
        cheapest_flight = db.session.query(func.min(Flight.fare_price)).scalar()

    if nDistances != 0:
        longest_stretch = distances.first()
        longest_stretch_flight = Flight.query.filter_by(
            destiny_airport=longest_stretch.destiny_airport
        ).first()
        longest_stretch_aircraft = longest_stretch_flight.aircraft
        longest_stretch_apo = longest_stretch.origin_airport
        longest_stretch_apd = longest_stretch.destiny_airport

    return render_template('index.html',
        nAirports=nAirports,
        nFlights=nFlights,
        nAircrafts=nAircrafts,
        nDistances=nDistances,
        aircrafts=aircrafts,
        expensive_flight=expensive_flight,
        cheapest_flight=cheapest_flight,
        longest_stretch=longest_stretch,
        longest_stretch_apo=longest_stretch_apo,
        longest_stretch_apd=longest_stretch_apd,
        longest_stretch_flight=longest_stretch_flight,
        longest_stretch_aircraft=longest_stretch_aircraft,
    )

@app.route('/clean_data')
def clean_data():
    print("Stretch_Cheapest_Option:" + str(Stretch_Cheapest_Option.query.delete()))
    print("Airports_Distance:" + str(Airports_Distance.query.delete()))
    print("Flight:" + str(Flight.query.delete()))
    print("Aircraft:" + str(Aircraft.query.delete()))
    print("Airport:" + str(Airport.query.delete()))

    try:
        db.session.commit()
    except Exception as e:
        return str(e)
    finally:
        return "ok"

@app.route('/load_airports_data')
def load_airports_data():
    clean_data()
    total = 0

    data = requests.get(os.environ['AIRPORTS_DATA_URL'], 
        auth=(os.environ['USER'], os.environ['PASSWORD']))

    json = data.json()

    for ap in json:
        obj = json[str(ap)]

        airport = Airport(obj)
        db.session.add(airport)

        try:
            db.session.commit()
            total += 1
        except Exception as e:
            return str(e)

    return total

def register_flight_data(origin, destiny, date):
    data = requests.get('http://stub.2xt.com.br/air/search/' + os.environ['APIKEY'] 
        + '/' + origin + '/' + destiny + '/' + str(date),
        auth=(os.environ['USER'], os.environ['PASSWORD']))

    json = data.json() 
    currency = json['summary']['currency']

    flights = json['options']

    for flt in flights:
        register_aircraft(flt['aircraft'])
        aircraft = Aircraft.query.filter(and_(Aircraft.model == flt['aircraft']['model'],
            Aircraft.manufacturer == flt['aircraft']['manufacturer'])).first()
        origin_obj = Airport.query.filter_by(iata=origin).first()
        destiny_obj = Airport.query.filter_by(iata=destiny).first()

        flight = Flight(
            departure_time=flt['departure_time'],
            arrival_time=flt['arrival_time'],
            fare_price=flt['fare_price'],
            currency=currency,
            aircraft=aircraft,
            origin_airport_id=origin_obj.id,
            destiny_airport_id=destiny_obj.id,
            requested_url=data.request.url,
        )

        db.session.add(flight)

        try:
            db.session.commit()
        except:
            db.session.rollback()

def register_aircraft(data):
    aicraft = Aircraft(data)

    db.session.add(aicraft)

    try:
        db.session.commit()
    except:
        db.session.rollback()

@app.route('/populate_flight_data/<selected_date>')
@app.route('/populate_flight_data/', defaults={'selected_date': None})
def populate_flight_data(selected_date):
    if selected_date is None:
        selected_date = datetime.now().date() - timedelta(days=40)

    try:
        selected_date = datetime.strptime(selected_date, "%Y-%m-%d").date()
    except:
        return "Invalid date. Please use one of this formats: YYYY-MM-DD,\
            YY-MM-DD, YYYY-M-D"
    finally:
        airports = Airport.query.all()
        nAirpots = len(airports)

        # permuting across airports
        for ap_origin in airports:
            origin = ap_origin.iata

            for ap_destiny in airports:
                destiny = ap_destiny.iata

                if origin != destiny:
                    register_flight_data(origin,destiny,selected_date)

        return redirect(url_for('index'))


def distance_calculator(origin, destiny):
    return str("%.4f" % geopy.distance.vincenty(origin, destiny).km)

@app.route('/calculate_distances/<direct>')
def calculate_distances(direct):
    airports = Airport.query.all()

    for index1, origin in enumerate(airports):
        for index2, destiny in enumerate(airports):
            if origin != destiny and index2 >= index1:
                distance = distance_calculator((origin.lat, origin.lon),
                    (destiny.lat, destiny.lon)
                )
                bt_distance = Airports_Distance(
                    origin_airport_id=origin.id,
                    destiny_airport_id=destiny.id,
                    distance=distance,
                )

                db.session.add(bt_distance)

                try:
                    db.session.commit()
                except:
                    db.session.rollback()

    if direct:
        return redirect(url_for('index'))
    else:
        return "All distances calculated"

def aircraft_average_speed(aircraft):
    flights = Flight.query.filter_by(aircraft=aircraft)
    fligth_time = timedelta(0)
    total_distance = 0.0

    for flight in flights:
        time_diff = flight.arrival_time - flight.departure_time
        fligth_time = fligth_time + time_diff
        ad = Airports_Distance.query.filter(
            or_(
                and_(
                    Airports_Distance.origin_airport_id == flight.origin_airport_id,
                    Airports_Distance.destiny_airport_id == flight.destiny_airport_id,
                ),
                and_(
                    Airports_Distance.origin_airport_id == flight.destiny_airport_id,
                    Airports_Distance.destiny_airport_id == flight.origin_airport_id,
                )
            )
        ).first()
        total_distance += ad.distance

    fligth_time_hours = fligth_time.days * 24 + fligth_time.seconds // 3600
    minutes = (fligth_time.seconds % 3600) // 60
    fligth_time_hours += minutes / 60
    aircraft.esteemed_speed = (total_distance/fligth_time_hours)

    try:
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        print(e)

def aircraft_average_fare_price(aircraft):
    flights = Flight.query.filter_by(aircraft=aircraft)
    total_fare = 0.0
    total_distance = 0.0

    for flight in flights:
        total_fare += flight.fare_price
        ad = Airports_Distance.query.filter(
            or_(
                and_(
                    Airports_Distance.origin_airport_id == flight.origin_airport_id,
                    Airports_Distance.destiny_airport_id == flight.destiny_airport_id,
                ),
                and_(
                    Airports_Distance.origin_airport_id == flight.destiny_airport_id,
                    Airports_Distance.destiny_airport_id == flight.origin_airport_id,
                )
            )
        ).first()
        total_distance += ad.distance

    aircraft.esteemed_fare_price = (total_fare/total_distance)

    try:
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        print(e)

@app.route('/calculate_aircraft_data')
def calculate_aircraft_data():
    distances = Airports_Distance.query.all()
    aircrafts = Aircraft.query.all()

    if len(distances) == 0:
        calculate_distances(None)

    for aircraft in aircrafts:
        aircraft_average_speed(aircraft)
        aircraft_average_fare_price(aircraft)

    return redirect(url_for('index'))

def register_cheapest_stretche(origin, destiny):
    flight = Flight.query.filter(and_(
        Flight.origin_airport_id == origin,
        Flight.destiny_airport_id == destiny,
        )
    ).order_by(Flight.fare_price).first()

    stretch = Stretch_Cheapest_Option(
        origin_airport_id=origin,
        destiny_airport_id=destiny,
        flight_id=flight.id,
    )
    db.session.add(stretch)

    try:
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        print(e)

@app.route('/calculate_cheapest_stretches')
def calculate_cheapest_stretches():
    ads = Airports_Distance.query.all()

    for ad in ads:
        register_cheapest_stretche(
            ad.origin_airport_id,
            ad.destiny_airport_id,
        )

        register_cheapest_stretche(
            ad.destiny_airport_id,
            ad.origin_airport_id,
        )


    return "ok"